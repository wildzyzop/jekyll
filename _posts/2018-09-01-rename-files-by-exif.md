---
layout: post
title:  "Переименование файлов в соответствии с датой из EXIF"
date:   2018-09-01 18:26:14 +0300
categories: restoredata exif jpeg jhead
---

## Переименование файлов в соответствии с датой из EXIF

После восстановления программой `PhotoRec` восстановленные файлы именуются по номеру начального кластера с файлом, что не очень удобно. При этом при наличии EXIF'a в восстанавливаемом файле `PhotoRec` ставит дату и время файла из EXIF. Привести имена файлов в более удобный вид можно с помощью программы `jhead` (есть в пакетах). 

Программа позволяет в том числе переименовывать файлы в соответствии с датой из EXIF.

```shell_session
apt-get install jhead
```
 
Разложить файлы по каталогам вида `ГГГГ_ММ` в текущем каталоге и дать им имена вида `IMG_ГГГГММДД-ЧЧММСС_NNNN.jpg` (где `NNNN` это сквозной счётик для всех обрабатываемых файлов):

```shell_session
jhead -n%Y_%m/IMG_%Y%m%d-%H%M%S_%02i *.jpg
```

---

Ссылки:

ExifTool – швейцарский нож фотометаданных / Хабр  
<https://habr.com/post/232267/#comment_7840781>

ExifToolGUI  
<http://u88.n24.queensu.ca/~bogdan/>

ExifTool by Phil Harvey  
<https://www.sno.phy.queensu.ca/~phil/exiftool/>

Exif Jpeg header manipulation tool  
<http://www.sentex.net/~mwandel/jhead/>